﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeVariableOnTrueOrFalse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Is the sky blue?");
            Console.WriteLine("Answer: TRUE or FALSE");
            var answer = bool.Parse(Console.ReadLine());
            var result = "";

            if (answer)
            {
                result = "You are correct!";
            }
            else
            {
                result = "Nah thats wrong bro";
            }
            Console.WriteLine(result);
        }
    }
}
